package com.emrah.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;

import java.util.Properties;

@Getter
@Setter
@NoArgsConstructor
public class JobLaunchRequest {
    private String name;
    private Properties jobParameterProperties;

    public JobParameters getJobParameters() {
        Properties properties = new Properties();
        properties.putAll(this.jobParameterProperties);
        return new JobParametersBuilder(properties).toJobParameters();
    }
}
